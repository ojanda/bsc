package cz.ojanda.service;

import cz.ojanda.model.PackageFee;
import cz.ojanda.model.PackageRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URL;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AppServiceTest {
    private AppService appService;

    @BeforeEach
    private void init() {
        appService = new AppService();
    }

    @Test
    void processArgs_givenEmptyFile_listIsNull() {
        URL dataPackage = getClass().getClassLoader().getResource("data_test_package_empty");
        URL dataFee = getClass().getClassLoader().getResource("data_test_fee_empty");
        appService.processArgs(new String[]{dataPackage.getPath(), dataFee.getPath()});

        List<PackageRecord> packageList = appService.getPackageService().getPackageList();
        assertNotNull(packageList);
        assertEquals(0, packageList.size());
        List<PackageFee> feeList = appService.getFeeService().getFeeList();
        assertNotNull(feeList);
        assertEquals(0, feeList.size());
    }

    @Test
    void processArgs_givenWrongFile_listIsNull() {
        appService.processArgs(new String[]{"data_test_wrong", "data_test_fee_wrong"});

        List<PackageRecord> packageList = appService.getPackageService().getPackageList();
        assertNotNull(packageList);
        assertEquals(0, packageList.size());
        List<PackageFee> feeList = appService.getFeeService().getFeeList();
        assertNotNull(feeList);
        assertEquals(0, feeList.size());
    }

    @Test
    void processArgs_givenWrongValidationFile_listIsNotNullWithSize2() {
        URL dataPackage = getClass().getClassLoader().getResource("data_test_package_wrongValidation");
        URL dataFee = getClass().getClassLoader().getResource("data_test_fee_wrongValidation");
        appService.processArgs(new String[]{dataPackage.getPath(), dataFee.getPath()});

        List<PackageRecord> packageList = appService.getPackageService().getPackageList();
        assertNotNull(packageList);
        assertEquals(2, packageList.size());
        List<PackageFee> feeList = appService.getFeeService().getFeeList();
        assertNotNull(feeList);
        assertEquals(4, feeList.size());
    }

    @Test
    void processArgs_givenOkFile_listIsNotNullWithSize4() {
        URL dataPackage = getClass().getClassLoader().getResource("data_test_package_ok");
        URL dataFee = getClass().getClassLoader().getResource("data_test_fee_ok");
        appService.processArgs(new String[]{dataPackage.getPath(), dataFee.getPath()});

        List<PackageRecord> packageList = appService.getPackageService().getPackageList();
        assertNotNull(packageList);
        assertEquals(4, packageList.size());
        List<PackageFee> feeList = appService.getFeeService().getFeeList();
        assertNotNull(feeList);
        assertEquals(7, feeList.size());
    }

    @Test
    void processInput_givenWrongInput_listSizeIs0() {
        appService.processInput("test");
        assertEquals(0, appService.getPackageService().getPackageList().size());

        appService.processInput("3.4 08801");
        appService.processInput("2 90005");
        assertEquals(2, appService.getPackageService().getPackageList().size());
    }

    @Test
    void processInput_givenOkInput_listSizeIs2() {
        appService.processInput("3.4 08801");
        appService.processInput("2 90005");
        assertEquals(2, appService.getPackageService().getPackageList().size());
    }
}