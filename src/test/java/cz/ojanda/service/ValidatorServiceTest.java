package cz.ojanda.service;

import cz.ojanda.model.PackageFee;
import cz.ojanda.model.PackageRecord;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ValidatorServiceTest {
    private ValidatorService validatorService = new ValidatorService();

    @Test
    void validateStringPackage() {
        assertFalse(validatorService.validateStringPackage(""));
        assertFalse(validatorService.validateStringPackage("asd"));
        assertFalse(validatorService.validateStringPackage("asd asd"));
        assertFalse(validatorService.validateStringPackage("asd asd asd"));
        assertFalse(validatorService.validateStringPackage("3,4 08801"));
        assertTrue(validatorService.validateStringPackage("3.4 08801"));
    }

    @Test
    void validateObjectPackage() {
        assertFalse(validatorService.validateObjectPackage(new PackageRecord(null, null)));
        assertFalse(validatorService.validateObjectPackage(new PackageRecord(null, BigDecimal.ONE)));
        assertFalse(validatorService.validateObjectPackage(new PackageRecord("123456", BigDecimal.ONE)));
        assertFalse(validatorService.validateObjectPackage(new PackageRecord("asd", BigDecimal.ONE)));
        assertFalse(validatorService.validateObjectPackage(new PackageRecord("08801", null)));
        assertFalse(validatorService.validateObjectPackage(new PackageRecord("08801", new BigDecimal("-1"))));
        assertFalse(validatorService.validateObjectPackage(new PackageRecord("08801", new BigDecimal("3.1234"))));
        assertTrue(validatorService.validateObjectPackage(new PackageRecord("08801", new BigDecimal("3.4"))));
    }

    @Test
    void validateStringFee() {
        assertFalse(validatorService.validateStringFee(""));
        assertFalse(validatorService.validateStringFee("asd"));
        assertFalse(validatorService.validateStringFee("asd asd"));
        assertFalse(validatorService.validateStringFee("asd asd asd"));
        assertFalse(validatorService.validateStringFee("10 5,00"));
        assertTrue(validatorService.validateStringFee("10 5.00"));
    }

    @Test
    void validateObjectFee() {
        assertFalse(validatorService.validateObjectFee(new PackageFee(null, null)));
        assertFalse(validatorService.validateObjectFee(new PackageFee(null, BigDecimal.ONE)));
        assertFalse(validatorService.validateObjectFee(new PackageFee(new BigDecimal("-1"), BigDecimal.ONE)));
        assertFalse(validatorService.validateObjectFee(new PackageFee(new BigDecimal("3.1234"), BigDecimal.ONE)));
        assertFalse(validatorService.validateObjectFee(new PackageFee(BigDecimal.ONE, new BigDecimal("-1"))));
        assertFalse(validatorService.validateObjectFee(new PackageFee(BigDecimal.ONE ,new BigDecimal("3.123"))));
        assertTrue(validatorService.validateObjectFee(new PackageFee(new BigDecimal("10") ,new BigDecimal("5.00"))));
    }
}