package cz.ojanda.service;

import cz.ojanda.model.PackageFee;
import cz.ojanda.model.PackageRecord;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class ParserTest {
    private Parser parser = new Parser();

    @Test
    void parsePackage_givenCorrectString_returnObject() {
        assertEquals(new PackageRecord("08801", new BigDecimal("3.4")), parser.parsePackage("3.4 08801"));
    }

    @Test
    void parsePackage_givenValidationError_returnNull() {
        assertNull(parser.parsePackage("3.4 088011"));
    }

    @Test
    void parseFee_givenCorrectString_returnObject() {
        assertEquals(new PackageFee(BigDecimal.TEN, new BigDecimal("5.00")), parser.parseFee("10 5.00"));
    }

    @Test
    void parseFee_givenValidationError_returnNull() {
        assertNull(parser.parseFee("10 5.0011"));
    }
}