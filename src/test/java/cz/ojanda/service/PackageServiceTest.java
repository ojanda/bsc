package cz.ojanda.service;

import cz.ojanda.model.PackageRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PackageServiceTest {
    private PackageService packageService;

    @BeforeEach
    private void init() {
        packageService = new PackageService();
    }

    @Test
    void addPackage_givenSamePostCode_listWithCorrectAddReturned() {
        packageService.addPackage(new PackageRecord("08801", new BigDecimal("3.4")));
        packageService.addPackage(new PackageRecord("08801", new BigDecimal("12.56")));

        List<PackageRecord> packageList = packageService.getPackageList();
        assertNotNull(packageList);
        assertEquals(1, packageList.size());
        assertTrue(packageList.contains(new PackageRecord("08801", new BigDecimal("15.96"))));
    }

    @Test
    void addPackage_givenNullPackage_listIsEmpty() {
        packageService.addPackage(null);

        List<PackageRecord> packageList = packageService.getPackageList();
        assertEquals(0, packageList.size());
    }

    @Test
    void getPackageList_givenPostCodes_listReturned() {
        packageService.addPackage(new PackageRecord("08801", new BigDecimal("3.4")));
        packageService.addPackage(new PackageRecord("90005", new BigDecimal("2")));
        packageService.addPackage(new PackageRecord("08801", new BigDecimal("12.56")));
        packageService.addPackage(new PackageRecord("08079", new BigDecimal("5.5")));
        packageService.addPackage(new PackageRecord("09300", new BigDecimal("3.2")));

        List<PackageRecord> packageList = packageService.getPackageList();
        assertNotNull(packageList);
        assertEquals(4, packageList.size());
        assertTrue(packageList.contains(new PackageRecord("08801", new BigDecimal("15.96"))));
        assertTrue(packageList.contains(new PackageRecord("90005", new BigDecimal("2"))));
        assertTrue(packageList.contains(new PackageRecord("08079", new BigDecimal("5.5"))));
        assertTrue(packageList.contains(new PackageRecord("09300", new BigDecimal("3.2"))));
    }
}