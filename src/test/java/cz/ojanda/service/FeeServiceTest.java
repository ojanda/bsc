package cz.ojanda.service;

import cz.ojanda.model.PackageFee;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FeeServiceTest {
    private FeeService feeService = new FeeService();

    @Test
    void addPackageFee() {
        feeService.addPackageFee(new PackageFee(BigDecimal.ONE, BigDecimal.TEN));

        List<PackageFee> feeList = feeService.getFeeList();
        assertNotNull(feeList);
        assertEquals(1, feeList.size());
    }

    @Test
    void addPackageFee_givenNullFee_listIsEmpty() {
        feeService.addPackageFee(null);

        List<PackageFee> feeList = feeService.getFeeList();
        assertEquals(0, feeList.size());
    }

    @Test
    void findFee() {
        feeService.addPackageFee(new PackageFee(new BigDecimal("10"), new BigDecimal("5.00")));
        feeService.addPackageFee(new PackageFee(new BigDecimal("5"), new BigDecimal("2.50")));
        feeService.addPackageFee(new PackageFee(new BigDecimal("3"), new BigDecimal("2.00")));
        feeService.addPackageFee(new PackageFee(new BigDecimal("2"), new BigDecimal("1.50")));
        feeService.addPackageFee(new PackageFee(new BigDecimal("1"), new BigDecimal("1.00")));
        feeService.addPackageFee(new PackageFee(new BigDecimal("0.5"), new BigDecimal("0.70")));
        feeService.addPackageFee(new PackageFee(new BigDecimal("0.2"), new BigDecimal("0.50")));

        assertEquals(new BigDecimal("5.00"), feeService.findFee(new BigDecimal("15.555")));
        assertEquals(new BigDecimal("2.50"), feeService.findFee(new BigDecimal("5.555")));
        assertEquals(new BigDecimal("0.70"), feeService.findFee(new BigDecimal("0.555")));
    }
}