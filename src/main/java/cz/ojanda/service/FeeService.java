package cz.ojanda.service;

import cz.ojanda.model.PackageFee;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Slf4j
public class FeeService {
    private Map<BigDecimal, BigDecimal> fees = new ConcurrentHashMap<>();

    public void addPackageFee(PackageFee packageFee) {
        if (packageFee != null) {
            fees.put(packageFee.getWeight(), packageFee.getFee());
            log.debug("Package fee add: " + packageFee.toString());
            System.out.println("Package fee add: " + packageFee.toString());
        }
    }

    public List<PackageFee> getFeeList() {
        return fees.entrySet().stream()
                .sorted(Map.Entry.comparingByKey(Comparator.reverseOrder()))
                .map(fee -> new PackageFee(fee.getKey(), fee.getValue()))
                .collect(Collectors.toList());
    }

    public BigDecimal findFee(BigDecimal weight) {
        return fees.entrySet().stream()
                .sorted(Map.Entry.comparingByKey(Comparator.reverseOrder()))
                .filter(fee -> weight.compareTo(fee.getKey()) >= 0)
                .findFirst()
                .map(Map.Entry::getValue)
                .orElse(null);
    }
}
