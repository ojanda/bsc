package cz.ojanda.service;

import cz.ojanda.model.PackageFee;
import cz.ojanda.model.PackageRecord;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.math.BigDecimal;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class ValidatorService {
    private static final String VALIDATION_ERROR_PREFIX = "Validation error: ";
    private static final String INPUT_PREFIX= " Input: ";

    private ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    private Validator validator = factory.getValidator();

    public boolean validateStringPackage(String input) {
        if (StringUtils.isBlank(input)) {
            logValidationError("input is empty.", input);
            return false;
        }
        String[] inputSplit = input.split(" ");
        if (inputSplit.length != 2) {
            logValidationError("input is in wrong format.", input);
            return false;
        }
        try {
            BigDecimal weight = new BigDecimal(inputSplit[0]);
        } catch (NumberFormatException exception) {
            logValidationError("input weight is not number or incorrect number format.", input);
            return false;
        }
        return true;
    }

    public boolean validateObjectPackage(PackageRecord packageRecord) {
        Set<ConstraintViolation<PackageRecord>> validations = validator.validate(packageRecord);

        if (validations.isEmpty()) {
            return true;
        }
        validations.stream().forEach(validation -> log.debug(VALIDATION_ERROR_PREFIX + validation.getMessage() + INPUT_PREFIX
                + validation.getInvalidValue()));
        return false;
    }

    public boolean validateStringFee(String input) {
        if (StringUtils.isBlank(input)) {
            logValidationError("input is empty.", input);
            return false;
        }
        String[] inputSplit = input.split(" ");
        if (inputSplit.length != 2) {
            logValidationError("input is in wrong format.", input);
            return false;
        }
        try {
            BigDecimal weight = new BigDecimal(inputSplit[0]);
        } catch (NumberFormatException exception) {
            logValidationError("input weight is not number or incorrect number format.", input);
            return false;
        }
        try {
            BigDecimal fee = new BigDecimal(inputSplit[1]);
        } catch (NumberFormatException exception) {
            logValidationError("input fee is not number or incorrect number format.", input);
            return false;
        }
        return true;
    }

    public boolean validateObjectFee(PackageFee packageFee) {
        Set<ConstraintViolation<PackageFee>> validations = validator.validate(packageFee);

        if (validations.isEmpty()) {
            return true;
        }
        validations.stream().forEach(validation -> log.debug(VALIDATION_ERROR_PREFIX + validation.getMessage() + INPUT_PREFIX
                + validation.getInvalidValue()));
        return false;
    }

    private void logValidationError(String message, String input) {
        String logMessage = VALIDATION_ERROR_PREFIX + message + INPUT_PREFIX + input;
        log.debug(logMessage);
    }
}
