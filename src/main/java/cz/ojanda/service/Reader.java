package cz.ojanda.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class Reader {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public String readFromConsole() {
        String input = null;
        try {
            input = reader.readLine();
        } catch (IOException e) {
            log.error("Error with reading input from console.", e);
            System.out.println("Error with reading input from console.");
        }
        return input;
    }

    public List<String> loadFromFile(String fileName) {
        if (StringUtils.isNotBlank(fileName)) {
            try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
                return stream.collect(Collectors.toList());
            } catch (Exception e) {
                log.error("Error with reading file: " + fileName, e);
                System.out.println("Error with reading file: " + fileName);
            }
        }
        return Collections.emptyList();
    }
}
