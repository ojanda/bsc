package cz.ojanda.service;

import cz.ojanda.model.PackageRecord;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Slf4j
public class PackageService {
    private Map<String, BigDecimal> packages = new ConcurrentHashMap<>();

    public List<PackageRecord> getPackageList() {
        return packages.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .map(packageRecord -> new PackageRecord(packageRecord.getKey(), packageRecord.getValue()))
                .collect(Collectors.toList());
    }

    public void addPackage(PackageRecord packageRecord) {
        if (packageRecord != null) {
            packages.merge(packageRecord.getPostCode(), packageRecord.getWeight(), BigDecimal::add);
            log.debug("Package add: " + packageRecord.toString());
            System.out.println("Package add: " + packageRecord.toString());
        }
    }
}
