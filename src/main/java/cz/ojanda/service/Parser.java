package cz.ojanda.service;

import cz.ojanda.model.PackageFee;
import cz.ojanda.model.PackageRecord;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.*;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class Parser {
    private static final String SPLIT_CHARACTER = " ";

    private ValidatorService validatorService = new ValidatorService();

    public PackageRecord parsePackage(String input) {
        if (validatorService.validateStringPackage(input)) {
            String[] inputSplit = input.split(SPLIT_CHARACTER);
            PackageRecord packageRecord = new PackageRecord(inputSplit[1], new BigDecimal(inputSplit[0]));
            if (validatorService.validateObjectPackage(packageRecord)) {
                return packageRecord;
            }
        }
        log.debug("Error parse package input.");
        System.out.println("Error parse package input. Accepted format: <weight: positive number, >0, maximal 3 decimal places, . (dot) " +
                "as decimal separator><space><postal code: fixed 5 digits>");
        return null;
    }

    public PackageFee parseFee(String input) {
        if (validatorService.validateStringFee(input)) {
            String[] inputSplit = input.split(SPLIT_CHARACTER);
            PackageFee packageFee = new PackageFee(new BigDecimal(inputSplit[0]), new BigDecimal(inputSplit[1]));
            if (validatorService.validateObjectFee(packageFee)) {
                return packageFee;
            }
        }
        log.debug("Error parse fee input.");
        System.out.println("Error parse fee input. Accepted format: <weight: positive number, >0, maximal 3 decimal places, . (dot) as " +
                "decimal separator><space><fee: positive number, >=0, fixed two decimals, . (dot) as decimal separator>");
        return null;
    }
}
