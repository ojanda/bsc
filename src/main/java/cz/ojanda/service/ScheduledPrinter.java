package cz.ojanda.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
public class ScheduledPrinter implements Runnable {
    private static final String SPACE_SEPARATOR = " ";

    PackageService packageService;
    FeeService feeService;

    @Override
    public void run() {
        if (!packageService.getPackageList().isEmpty()) {
            System.out.println();
            System.out.println("--- PACKAGES LIST ---");
            packageService.getPackageList().stream()
                    .forEach(packageRow -> {
                        BigDecimal fee = feeService.findFee(packageRow.getWeight());
                        String feeString = fee != null ? SPACE_SEPARATOR + fee.setScale(2) : StringUtils.EMPTY;
                        System.out.println(packageRow.getPostCode() + SPACE_SEPARATOR + packageRow.getWeight().setScale(3) + feeString);
                    });
            System.out.println("---------------------");
        }
    }
}
