package cz.ojanda.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppService {
    private static final String EXIT_COMMAND = "quit";

    private Reader reader = new Reader();
    private Parser parser = new Parser();
    private PackageService packageService = new PackageService();
    private FeeService feeService = new FeeService();
    private boolean running = true;

    public void processArgs(String[] args) {
        if (args.length >= 1) {
            String fileName = args[0];

            System.out.println("Loading from file: " + fileName);
            reader.loadFromFile(fileName).stream()
                    .forEach(record -> packageService.addPackage(parser.parsePackage(record)));
        }

        if (args.length == 2) {
            String fileName = args[1];

            System.out.println("Loading from file: " + fileName);
            reader.loadFromFile(fileName).stream()
                    .forEach(record -> feeService.addPackageFee(parser.parseFee(record)));
        }
    }

    public void start() {
        while (running) {
            String input = reader.readFromConsole();
            processInput(input);
        }
    }

    public void processInput(String input) {
        if (EXIT_COMMAND.equalsIgnoreCase(input)) {
            running = false;
        } else {
            packageService.addPackage(parser.parsePackage(input));
        }
    }
}
