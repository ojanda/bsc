package cz.ojanda;

import cz.ojanda.service.*;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Main class of the application.
 */
@Slf4j
public class App
{
    public static void main( String[] args ) {
        log.debug("Application has started.");

        AppService app = new AppService();
        app.processArgs(args);

        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(new ScheduledPrinter(app.getPackageService(), app.getFeeService()), 0, 60,
                TimeUnit.SECONDS);

        app.start();

        log.debug("Application has stopped.");
        System.exit(0);
    }
}
