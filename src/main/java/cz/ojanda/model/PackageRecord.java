package cz.ojanda.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PackageRecord {
    @NotNull(message = "postCode is empty.")
    @Size(max = 5,message = "postCode does not have size 5 characters.")
    @Pattern(regexp = "\\d+", message = "postCode is not number.")
    private String postCode;

    @NotNull(message = "weight is empty.")
    @DecimalMin(value = "0.0", inclusive = false, message = "weight is less than 0.")
    @Digits(integer = 10, fraction = 3, message = "weight has the wrong number decimal places.")
    private BigDecimal weight;

    @Override
    public String toString() {
        return "postCode=" + postCode + ", weight=" + weight;
    }
}
