package cz.ojanda.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PackageFee {
    @NotNull(message = "weight is empty.")
    @DecimalMin(value = "0.0", inclusive = false, message = "weight is less than 0.")
    @Digits(integer = 10, fraction = 3, message = "weight has the wrong number decimal places.")
    private BigDecimal weight;

    @NotNull(message = "fee is empty.")
    @DecimalMin(value = "0.0", message = "fee is less than 0.")
    @Digits(integer = 10, fraction = 2, message = "fee has the wrong number decimal places.")
    private BigDecimal fee;

    @Override
    public String toString() {
        return "weight=" + weight + ", fee=" + fee;
    }
}
