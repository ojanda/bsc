## Build:
Use maven

``
mvn clean package
``

## Run

``java -jar bsc.jar [packagesFileName] [feeFileName]``

For example with packaged data in project:

``java -jar bsc.jar classes/data_ok classes/data_fee``

You can use data: ``data_ok, data_wrong, data_empty`` for packages.
You can use data: ``data_fee, data_fee_empty`` for packages fee.



## Validations
Only general message and correct usage is printed to the console. Specific messages are visible in the log. Path is ``logs/logFile.log``